﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace JsonToCsv
{
    class Program
    {
        private static string masterPath = @"C:\Users\Dana\source\repos\JsonToCsv\JsonToCsv";
        static void Main(string[] args)
        {
            var dt = ConvertJsonToDatatable(GetTextContents());
            var csvString = ToCsv(dt);
            File.WriteAllText(masterPath + @"\Output\myfile.csv", csvString, Encoding.UTF8);
        }

        public static string GetTextContents()
        {
            string path = masterPath + @"\xxd.json";
            string contents = string.Empty;

            // This text is added only once to the file.
            if (File.Exists(path))
            { // Open the file to read from.
                contents = File.ReadAllText(path);
                //Console.WriteLine(contents);
            }

            return contents;
        }
        public static DataTable ConvertJsonToDatatable(string jsonString)
        {
            var jsonLinq = JObject.Parse(jsonString);

            // Find the first array using Linq
            var linqArray = jsonLinq.Descendants().First(x => x is JArray);
            var jsonArray = new JArray();
            foreach (JObject row in linqArray.Children<JObject>())
            {
                var createRow = new JObject();
                foreach (JProperty column in row.Properties())
                {
                    // Only include JValue types
                    if (column.Value is JValue)
                    {
                        createRow.Add(column.Name, column.Value);
                    }
                }
                jsonArray.Add(createRow);
            }

            return JsonConvert.DeserializeObject<DataTable>(jsonArray.ToString());
        }

        public static string ToCsv(DataTable dataTable)
        {
            StringBuilder sbData = new StringBuilder();

            // Only return Null if there is no structure.
            if (dataTable.Columns.Count == 0)
                return null;

            foreach (var col in dataTable.Columns)
            {
                if (col == null)
                    sbData.Append(",");
                else
                    sbData.Append("\"" + col.ToString().Replace("\"", "\"\"") + "\",");
            }

            sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);

            foreach (DataRow dr in dataTable.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    if (column == null)
                        sbData.Append(",");
                    else
                        sbData.Append("\"" + column.ToString().Replace("\"", "\"\"") + "\",");
                }
                sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);
            }

            return sbData.ToString();
        }
    }
}
