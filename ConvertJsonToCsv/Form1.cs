﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace ConvertJsonToCsv
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                lblInput.Text = ofd.FileName;
              
            }
        }

        public static string GetTextContents(string inputPath)
        {
            var contents = string.Empty;

            // This text is added only once to the file.
            if (File.Exists(inputPath))
            { // Open the file to read from.
                contents = File.ReadAllText(inputPath);
                //Console.WriteLine(contents);
            }

            return contents.Replace("'","");
        }
        public static DataTable ConvertJsonToDatatable(string jsonString)
        {
            var jsonArray = new JArray();
            DataTable dt = new DataTable();
            try
            {
                dt = (DataTable) JsonConvert.DeserializeObject(jsonString, (typeof(DataTable)));
              //  var jsonLinq = JObject.Parse(jsonString);

                //// Find the first array using Linq
                //var linqArray = jsonLinq.Descendants().First(x => x is JArray);
                // foreach (JObject row in linqArray.Children<JObject>())
                //{
                //    var createRow = new JObject();
                //    foreach (JProperty column in row.Properties())
                //    {
                //        // Only include JValue types
                //        if (column.Value is JValue)
                //        {
                //            createRow.Add(column.Name, column.Value);
                //        }
                //    }

                //    jsonArray.Add(createRow);
                //}
            }
            catch (Exception ee)
            {
                MessageBox.Show("error", @"There is a problem with the file.  Validate the format at jsonlint.com" + ee.Message);
            }

            return dt;
        }

        public static string ToCsv(DataTable dataTable)
        {
            StringBuilder sbData = new StringBuilder();

            // Only return Null if there is no structure.
            if (dataTable.Columns.Count == 0)
                return null;

            foreach (var col in dataTable.Columns)
            {
                if (col == null)
                    sbData.Append(",");
                else
                    sbData.Append("\"" + col.ToString().Replace("\"", "\"\"") + "\",");
            }

            sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);

            foreach (DataRow dr in dataTable.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    if (column == null)
                        sbData.Append(",");
                    else
                        sbData.Append("\"" + column.ToString().Replace("\"", "\"\"") + "\",");
                }
                sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);
            }

            return sbData.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                lblOutput.Text = fbd.SelectedPath;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var inputFileName = lblInput.Text;
            var outputFileLocation = lblOutput.Text;
            var dt = ConvertJsonToDatatable(GetTextContents(inputFileName));
            string csvString = ToCsv(dt);

            File.WriteAllText(outputFileLocation + @"\jsonOutput.csv", csvString, Encoding.UTF8);
            lblResult.Text = "File created at: " + @outputFileLocation;

        }
    }
}
